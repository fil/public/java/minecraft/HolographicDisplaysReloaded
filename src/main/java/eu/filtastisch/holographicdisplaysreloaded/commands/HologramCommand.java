package eu.filtastisch.holographicdisplaysreloaded.commands;

import de.thesourcecoders.capi.spigot.GuiManager;
import de.thesourcecoders.capi.spigot.ItemBuilder;
import de.thesourcecoders.capi.util.PageUtils;
import eu.filtastisch.holographicdisplaysreloaded.HolographicDisplaysReloaded;
import eu.filtastisch.holographicdisplaysreloaded.types.Hologram;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.event.ClickEvent;
import net.kyori.adventure.text.event.HoverEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.TextDisplay;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class HologramCommand implements CommandExecutor {

    private final HolographicDisplaysReloaded plugin = HolographicDisplaysReloaded.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String aliases, String[] args) {
        if (sender instanceof Player p) {
            if (p.hasPermission("holographicdisplays.command")) {
                if (args.length == 0) {
                    this.sendHelp(p, "32");
                } else if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("help")) {
                        this.sendHelp(p, "35");
                    } else if (args[0].equalsIgnoreCase("list")) {
                        this.list(p, 1);
                    } else if (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("rl")) {
                        this.reload(p);
                    } else if (args[0].equalsIgnoreCase("edit")) {
                        this.edit(p);
                    } else {
                        this.sendHelp(p, "43");
                    }
                } else if (args.length == 2) {
                    if (args[0].equalsIgnoreCase("delete")) {
                        this.delete(p, args[1]);
                    } else if (args[0].equalsIgnoreCase("near")) {
                        try {
                            int r = Integer.parseInt(args[1]);
                            this.near(p, r);
                        } catch (NumberFormatException ignored) {
                            this.sendInvalidFormat(p);
                        }
                    } else if (args[0].equalsIgnoreCase("list")) {
                        try {
                            int i = Integer.parseInt(args[1]);
                            this.list(p, i);
                        } catch (NumberFormatException ignored) {
                            this.sendInvalidFormat(p);
                        }
                    } else if (args[0].equalsIgnoreCase("teleport")) {
                        this.teleport(p, args[1]);
                    } else if (args[0].equalsIgnoreCase("edit")) {
                        this.edit(p, args[1]);
                    } else if (args[0].equalsIgnoreCase("movehere")) {
                        this.moveHere(p, args[1]);
                    } else if (args[0].equalsIgnoreCase("info")) {
                        this.info(p, args[1]);
                    } else {
                        this.sendHelp(p, "75");
                    }
                } else {
                    if (args.length == 3) {
                        if (args[0].equalsIgnoreCase("create")) {
                            this.createHandler(p, args[1], Arrays.copyOfRange(args, 2, args.length));
                        } else if (args[0].equalsIgnoreCase("removeline")) {
                            try {
                                int i = Integer.parseInt(args[2]);
                                this.removeLine(p, args[1], i);
                            } catch (NumberFormatException ignored) {
                                this.sendInvalidFormat(p);
                            }
                        } else if (args[0].equalsIgnoreCase("copy")) {
                            this.copy(p, args[1], args[2]);
                        } else if (args[0].equalsIgnoreCase("addline")) {
                            this.addLine(p, args[1], Arrays.copyOfRange(args, 2, args.length));
                        } else {
                            this.sendHelp(p, "94");
                        }
                    } else {
                        if (args[0].equalsIgnoreCase("create")) {
                            this.createHandler(p, args[1], Arrays.copyOfRange(args, 2, args.length));
                        } else if (args[0].equalsIgnoreCase("addline")) {
                            this.addLine(p, args[1], Arrays.copyOfRange(args, 2, args.length));
                        } else if (args[0].equalsIgnoreCase("setline")) {
                            try {
                                int i = Integer.parseInt(args[2]);
                                this.setLine(p, args[1], i, Arrays.copyOfRange(args, 3, args.length));
                            } catch (NumberFormatException ignored) {
                                this.sendInvalidFormat(p);
                            }
                        } else if (args[0].equalsIgnoreCase("insertline")) {
                            try {
                                int i = Integer.parseInt(args[2]);
                                this.insert(p, args[1], i, Arrays.copyOfRange(args, 3, args.length));
                            } catch (NumberFormatException ignored) {
                                this.sendInvalidFormat(p);
                            }
                        } else {
                            this.sendHelp(p, "116");
                        }
                    }
                }
            }
        }
        return false;
    }

    public void createHandler(Player p, String name, String... text) {
        if (p.hasPermission("holographicdisplays.command.create")) {
            if (!Hologram.exists(name)) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < text.length; i++) {
                    if (i == text.length - 1) {
                        sb.append(text[i].replace('&', '§'));
                    } else {
                        sb.append(text[i].replace('&', '§')).append(" ");
                    }
                }
                String holoText = sb.toString();

                String hoverText = "§a§lHologram: §e" + name + "\n\n" + "§r" +
                        holoText;

                Hologram holo = new Hologram(name, p.getLocation().clone().add(0, 1.3, 0));
                holo.addLine(holoText);
                holo.spawnHologram();

                Component message = Component.text(plugin.getPrefix() + "§aYou created §e" + name).hoverEvent(HoverEvent.showText(Component.text(hoverText)));
                p.sendMessage(message);
            } else {
                p.sendMessage(Component.text("§cThis Hologram already exists!"));
            }
        }
    }

    public void delete(Player p, String name) {
        if (p.hasPermission("holographicdisplays.command.delete")) {
            if (Hologram.exists(name)) {
                Hologram hologram = Hologram.getFromName(name);
                if (hologram == null) {
                    p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                    return;
                }

                Hologram.removeHologram(hologram);

                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.append("§a§lHologram: §e").append(name).append("\n\n");

                for (String c : hologram.getLines())
                    stringBuilder.append("§r").append(c).append("\n");

                Component message = Component.text(plugin.getPrefix() + "§aYou deleted §e" + name).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
                p.sendMessage(message);
            } else {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
            }
        }

    }

    public void list(Player p, int page) {
        if (p.hasPermission("holographicdisplays.command.list")) {
            p.sendMessage(Component.text("§7§m--------§r §bList of all existing Holograms  §7§m--------"));
            PageUtils<String> holos = new PageUtils<>(this.plugin.getHoloNameList(), 9);
            int currentIndex = 0;

            for (String c : holos.getPageContent(page)) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("§eClick to Teleport!").append("\n\n");
                stringBuilder.append("§a§lHologram: §e").append(c).append("\n\n");
                for (String lines : Hologram.getFromName(c).getLines()) {
                    stringBuilder.append(lines).append("\n");
                }
                if (currentIndex == holos.getPageContent(page).size() - 1) {
                    p.sendMessage(Component.text("§r §a└ §e" + c).clickEvent(ClickEvent.clickEvent(ClickEvent.Action.RUN_COMMAND, "/holographicdisplays_9DxkiUmZxH1kukVObceOhJGlSgm5Z5 " + c)).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString()))));
                } else {
                    p.sendMessage(Component.text("§r §a├ §e" + c).clickEvent(ClickEvent.clickEvent(ClickEvent.Action.RUN_COMMAND, "/holographicdisplays_9DxkiUmZxH1kukVObceOhJGlSgm5Z5 " + c)).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString()))));
                }
                currentIndex++;
            }
        }
    }

    public void near(Player p, int radius) {
        if (p.hasPermission("holographicdisplays.command.near")) {
            Map<String, Location> holos = new HashMap<>();
            Set<String> unique = new HashSet<>();
            for (Entity e : p.getNearbyEntities(radius, radius, radius)) {
                Hologram holo = Hologram.getFromLocation(e.getLocation());

                if (e instanceof TextDisplay) {
                    String tag = e.getScoreboardTags().iterator().next();
                    if (e.getScoreboardTags().contains("hologram")) {
                        if (!unique.contains(tag)) {
                            holos.put(tag, Hologram.getFromName(tag).getLocation());
                            unique.add(tag);
                        }
                    }
                }
            }
            GuiManager inv = new GuiManager("§2Nearby Holograms", 5 * 9);
            int index = 0;
            for (String holoName : holos.keySet()) {
                Location loc = holos.get(holoName);
                ItemStack item = new ItemBuilder(Material.PAPER).setName("§a" + holoName).create();
                GuiManager.InventoryItem invItem = new GuiManager.InventoryItem(item, index, true, e -> p.teleport(loc));
                inv.setItems(invItem);
                index++;
            }
            inv.open(p);
        }
    }

    public void teleport(Player p, String name) {
        if (p.hasPermission("holographicdisplays.command.movehere"))
            p.teleport(Hologram.getFromName(name).getLocation());

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("§a§lHologram: §e").append(name).append("\n\n");

        for (String c : Hologram.getFromName(name).getLines())
            stringBuilder.append("§r").append(c).append("\n");

        Component message = Component.text(plugin.getPrefix() + "§aYou have been teleported to §e" + name).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
        p.sendMessage(message);
    }

    public void moveHere(Player p, String hologram) {
        if (p.hasPermission("holographicdisplays.command.movehere")) {
            Hologram holo = Hologram.getFromName(hologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }
            holo.setLocation(p.getLocation().clone().add(0, 1.3, 0), true);
            holo.update();

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("§a§lHologram: §e").append(hologram).append("\n\n").append("§r");

            for (String c : holo.getLines())
                stringBuilder.append("§r").append(c).append("\n");

            Component message = Component.text(plugin.getPrefix() + "§aYou moved §e" + hologram + " §ato you Location").hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
            p.sendMessage(message);
        }
    }

    public void edit(Player p, String hologram) {
        if (p.hasPermission("holographicdisplays.command.edit")) {
            p.sendMessage(Component.text(plugin.getPrefix() + "§cStill in development. Available on Release 1.2.0"));
        }
    }

    public void edit(Player p) {
        if (p.hasPermission("holographicdisplays.command.edit")) {
            if (p.getUniqueId().equals(UUID.fromString("1cd03978-b2d7-49e1-a8ed-65e5fda7bc42"))) {
                Inventory inv = Bukkit.createInventory(p, 9 * 6, Component.text("§2Holograms"));
                ItemStack filler = new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create();

                int rows = inv.getSize() / 9;

                int i;
                for (i = 0; i < 9; ++i) {
                    inv.setItem(i, filler);
                    inv.setItem(i + inv.getSize() - 9, filler);
                }

                for (i = 0; i < rows; ++i) {
                    inv.setItem(9 * i, filler);
                    inv.setItem(9 * i + 8, filler);
                }

                for (Hologram c : Hologram.all) {
                    ItemStack item = new ItemBuilder(Material.PAPER).setName("§a" + c.getName()).create();
                    inv.addItem(item);
                }

                p.openInventory(inv);
            } else {
                p.sendMessage(Component.text(plugin.getPrefix() + "§cStill in development. Available on Release 1.2.0"));
            }
        }
    }

    public void addLine(Player p, String hologram, String... text) {
        if (p.hasPermission("holographicdisplays.command.addline")) {
            if (Hologram.exists(hologram)) {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < text.length; i++) {
                    if (i == text.length - 1) {
                        sb.append(text[i].replace('&', '§'));
                    } else {
                        sb.append(text[i].replace('&', '§')).append(" ");
                    }
                }
                String holoText = sb.toString();

                Hologram holo = Hologram.getFromName(hologram);
                if (holo == null) {
                    p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                    return;
                }
                holo.addLine(holoText);
                holo.update();

                StringBuilder stringBuilder = new StringBuilder();

                stringBuilder.append("§a§lHologram: §e").append(hologram).append("\n\n").append("§r");

                for (String c : holo.getLines())
                    stringBuilder.append("§r").append(c).append("\n");

                Component message = Component.text(plugin.getPrefix() + "§aYou added a line to §e" + hologram).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
                p.sendMessage(message);
            } else {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
            }
        }
    }

    public void removeLine(Player p, String hologram, int line) {
        if (p.hasPermission("holographicdisplays.command.removeline")) {
            Hologram holo = Hologram.getFromName(hologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }
            holo.getLines().remove(line);
            holo.update();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("§a§lHologram: §e").append(hologram).append("\n\n").append("§r");
            for (String c : holo.getLines())
                stringBuilder.append("§r").append(c).append("\n");

            Component message = Component.text(plugin.getPrefix() + "§aYou removed a line from §e" + hologram).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
            p.sendMessage(message);
        }
    }

    public void setLine(Player p, String hologram, int line, String... text) {
        if (p.hasPermission("holographicdisplays.command.setline")) {
            Hologram holo = Hologram.getFromName(hologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }
            StringBuilder sb = new StringBuilder();

            for (String c : text) {
                sb.append(c.replace('&', '§')).append(" ");
            }

            holo.setLine(sb.toString(), line);
            holo.update();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("§a§lHologram: §e").append(hologram).append("\n\n").append("§r");
            for (String c : holo.getLines())
                stringBuilder.append("§r").append(c).append("\n");

            Component message = Component.text(plugin.getPrefix() + "§aYou set a new content to §e" + hologram + " §aon line: §e" + line).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
            p.sendMessage(message);
        }
    }

    public void insert(Player p, String hologram, int line, String... text) {
        if (p.hasPermission("holographicdisplays.command.insert")) {
            Hologram holo = Hologram.getFromName(hologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }
            StringBuilder sb = new StringBuilder();

            for (String c : text) {
                sb.append(c.replace('&', '§')).append(" ");
            }
            holo.insertLine(sb.toString(), line);
            holo.update();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("§a§lHologram: §e").append(hologram).append("\n\n").append("§r");
            for (String c : holo.getLines())
                stringBuilder.append("§r").append(c).append("\n");

            Component message = Component.text(plugin.getPrefix() + "§aYou inserted a new line to §e" + hologram).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
            p.sendMessage(message);
        }
    }

    public void info(Player p, String hologram) {
        if (p.hasPermission("holographicdisplays.command.info")) {
            Hologram holo = Hologram.getFromName(hologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }
            StringBuilder sb = new StringBuilder("§7§m--------§r §bHolographic Displays info for §a" + hologram + " §7§m--------").append("\n");
            sb.append("§aLocation §eX: ").append(holo.getLocation().getBlockX()).append(" Y: ").append(holo.getLocation().getBlockY()).append(" Z: ").append(holo.getLocation().getBlockZ()).append("\n");
            sb.append("§aLines: §e").append("\n");
            for (String line : holo.getLines())
                sb.append(" ").append(line).append("\n");
            sb.append("§aShadow: §e").append(holo.isShadow()).append("\n");
            sb.append("§aBackground: §e").append(holo.isBackground()).append("\n");
            sb.append("§aLineSpace: §e").append(holo.getLineSpacing()).append("\n");
            sb.append("§aOpacity: §e").append(holo.getOpacity()).append("\n");
            sb.append("§aBillboard: §e").append(holo.getBillboard().name()).append("\n");
            sb.append("§aText Alignment: §e").append(holo.getTextAlignment().name()).append("\n");
            sb.append("§aColor: §e").append(holo.getBackgroundColor().asARGB()).append("\n");

            p.sendMessage(Component.text(sb.toString()));
        }
    }

    public void copy(Player p, String fromHologram, String toHologram) {
        if (p.hasPermission("holographicdisplays.command.copy")) {
            Hologram holo = Hologram.getFromName(fromHologram);
            if (holo == null) {
                p.sendMessage(Component.text(this.plugin.getPrefix() + "§cThis Hologram doesnt exist!"));
                return;
            }

            Hologram newHolo = new Hologram(toHologram, p.getLocation(), holo.getLines(), holo.getLineSpacing(), true);
            newHolo.setBackground(holo.isBackground());
            newHolo.setBillboard(holo.getBillboard());
            newHolo.setOpacity(holo.getOpacity());
            newHolo.setShadow(holo.isShadow());
            newHolo.setTextAlignment(holo.getTextAlignment());
            newHolo.setBackgroundColor(holo.getBackgroundColor());
            newHolo.setSeeThrough(holo.isSeeThrough());

            newHolo.update();

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("§a§lHologram: §e").append(fromHologram).append("\n\n");
            for (String c : holo.getLines())
                stringBuilder.append("§r").append(c).append("\n");

            Component message = Component.text(plugin.getPrefix() + "§aYou copied §e" + fromHologram).hoverEvent(HoverEvent.showText(Component.text(stringBuilder.toString())));
            p.sendMessage(message);
        }
    }

    public void reload(Player p) {
        if (p.hasPermission("holographicdisplays.command.reload")) {

            List<Hologram> tempHoloList = new ArrayList<>(Hologram.all);

            for (Hologram c : tempHoloList) {
                c.despawn();
                Hologram.all.remove(c);
            }

            this.plugin.loadHologramsLater();

            p.sendMessage(Component.text(this.plugin.getPrefix() + "§aAll holograms has been reloaded from config!"));
        }
    }

    public void sendHelp(Player p, String line) {
        if (p.hasPermission("holographicdisplays.command.help")) {
            String builder = "§7§m-------- §bHolographic Displays Help §7§m--------" + "\n" +
                    "§a/hd help" + "\n" +
                    "§a/hd create <hologram> <text of first Line>" + "\n" +
                    "§a/hd delete <hologram>" + "\n" +
                    "§a/hd list <page>" + "\n" +
                    "§a/hd near <radius>" + "\n" +
                    "§a/hd teleport <hologram>" + "\n" +
                    "§a/hd movehere <hologram>" + "\n" +
                    "§a/hd addline <hologram> <text>" + "\n" +
                    "§a/hd removeline <hologram> <number>" + "\n" +
                    "§a/hd setline <hologram> <number> <new text>" + "\n" +
                    "§a/hd insertline <hologram> <number> <new text>" + "\n" +
                    "§a/hd info <hologram>" + "\n" +
                    "§a/hd copy <from hologram> <to hologram>" + "\n" +
                    "§a/hd reload" + "\n";
            //"§cFrom Line " + line;

            p.sendMessage(Component.text(builder));
        }
    }

    public void sendInvalidFormat(Player p) {

    }

}
