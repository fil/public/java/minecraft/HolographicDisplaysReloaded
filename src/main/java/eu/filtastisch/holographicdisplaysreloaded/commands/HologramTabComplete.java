package eu.filtastisch.holographicdisplaysreloaded.commands;

import eu.filtastisch.holographicdisplaysreloaded.HolographicDisplaysReloaded;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class HologramTabComplete implements TabCompleter {

    private final HolographicDisplaysReloaded plugin = HolographicDisplaysReloaded.getInstance();
    private final String[] argsOne = new String[]{"help", "create", "delete", "list", "near", "teleport", "movehere", "addline", "removeline", "setline", "insertline", "info", "copy"};

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if (args.length == 1) {
            return List.of(this.argsOne);
        }

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("create") ||
                    args[0].equalsIgnoreCase("delete") ||
                    args[0].equalsIgnoreCase("teleport") ||
                    args[0].equalsIgnoreCase("movehere") ||
                    args[0].equalsIgnoreCase("addline") ||
                    args[0].equalsIgnoreCase("removeline") ||
                    args[0].equalsIgnoreCase("setline") ||
                    args[0].equalsIgnoreCase("insertline") ||
                    args[0].equalsIgnoreCase("info") ||
                    args[0].equalsIgnoreCase("copy")) {
                return this.plugin.getHoloNameList();
            }
        }
        return null;
    }
}
