package eu.filtastisch.holographicdisplaysreloaded.types;

import eu.filtastisch.holographicdisplaysreloaded.HolographicDisplaysReloaded;
import lombok.Getter;
import lombok.Setter;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Display;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.TextDisplay;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class Hologram {

    private final String name;
    private Location location;

    private final List<String> lines;

    private List<TextDisplay> holos;

    private byte opacity;
    private boolean shadow;
    private boolean background;
    private boolean seeThrough;
    private TextDisplay.TextAlignment textAlignment;
    private Color backgroundColor;
    private double lineSpacing;
    private Display.Billboard billboard;

    private final HolographicDisplaysReloaded plugin = HolographicDisplaysReloaded.getInstance();

    public static List<Hologram> all = new ArrayList<>();

    public Hologram(String name, Location location) {
        this.name = name;
        this.location = location;
        this.lines = new ArrayList<>();
        this.holos = new ArrayList<>();

        this.plugin.getHoloNameList().add(this.name);

        this.opacity = this.plugin.getOpacity().get(0);
        this.shadow = this.plugin.isShadow();
        this.background = this.plugin.isBackground();
        this.seeThrough = this.plugin.isSeeThrough();
        this.textAlignment = TextDisplay.TextAlignment.valueOf(this.plugin.getTextAlign());
        this.backgroundColor = Color.fromARGB(this.plugin.getBackgroundColor());
        this.lineSpacing = this.plugin.getLineSpacing();
        this.billboard = Display.Billboard.valueOf(this.plugin.getBillboard());

        this.plugin.getHoloConfig().setLocation("holo." + this.name + ".location", this.location);
        this.plugin.getHoloConfig().set("holo." + name + ".lines", this.lines);
        this.plugin.getHoloConfig().set("holo." + name + ".opacity", new Byte[]{this.opacity});
        this.plugin.getHoloConfig().set("holo." + name + ".shadow", this.shadow);
        this.plugin.getHoloConfig().set("holo." + name + ".background", this.background);
        this.plugin.getHoloConfig().set("holo." + name + ".seeThrough", this.seeThrough);
        this.plugin.getHoloConfig().set("holo." + name + ".textAlignment", this.textAlignment.name());
        this.plugin.getHoloConfig().set("holo." + name + ".backgroundColor", this.backgroundColor.asARGB());
        this.plugin.getHoloConfig().set("holo." + name + ".lineSpacing", this.lineSpacing);
        this.plugin.getHoloConfig().set("holo." + name + ".billboard", this.billboard.name());
        this.plugin.getHoloConfig().save();

        if (!exists(name))
            all.add(this);
    }

    public Hologram(String name, Location location, List<String> lines, double lineSpacing, boolean safeToConfig) {
        this.name = name;
        this.location = location;
        this.lines = lines;
        this.holos = new ArrayList<>();
        this.lineSpacing = lineSpacing;

        this.plugin.getHoloNameList().add(this.name);

        this.opacity = this.plugin.getOpacity().get(0);
        this.shadow = this.plugin.isShadow();
        this.background = this.plugin.isBackground();
        this.seeThrough = this.plugin.isSeeThrough();
        this.textAlignment = TextDisplay.TextAlignment.valueOf(this.plugin.getTextAlign());
        this.backgroundColor = Color.fromARGB(this.plugin.getBackgroundColor());
        this.billboard = Display.Billboard.valueOf(this.plugin.getBillboard());

        if (safeToConfig) {
            this.plugin.getHoloConfig().setLocation("holo." + this.name + ".location", this.location);
            this.plugin.getHoloConfig().set("holo." + name + ".lines", this.lines);
            this.plugin.getHoloConfig().set("holo." + name + ".opacity", new Byte[]{this.opacity});
            this.plugin.getHoloConfig().set("holo." + name + ".shadow", this.shadow);
            this.plugin.getHoloConfig().set("holo." + name + ".background", this.background);
            this.plugin.getHoloConfig().set("holo." + name + ".seeThrough", this.seeThrough);
            this.plugin.getHoloConfig().set("holo." + name + ".textAlignment", this.textAlignment.name());
            this.plugin.getHoloConfig().set("holo." + name + ".backgroundColor", this.backgroundColor.asARGB());
            this.plugin.getHoloConfig().set("holo." + name + ".lineSpacing", this.lineSpacing);
            this.plugin.getHoloConfig().set("holo." + name + ".billboard", this.billboard.name());
            this.plugin.getHoloConfig().save();
        }

        if (!exists(name))
            all.add(this);
    }

    public static Hologram getFromName(String name) {
        for (Hologram c : all) {
            if (c.getName().equals(name))
                return c;
        }
        return null;
    }

    public static Hologram getFromLocation(Location loc) {
        for (Hologram c : all) {
            if (c.getLocation().equals(loc))
                return c;
        }
        return null;
    }

    public static boolean exists(String name) {
        return all.stream().anyMatch(current -> current.getName().equals(name));
    }

    public Hologram addLine(String line) {
        this.lines.add(line);
        this.plugin.getHoloConfig().setStringList("holo." + this.name + ".lines", this.lines.toArray(new String[lines.size()]));
        this.plugin.getHoloConfig().save();
        return this;
    }

    public Hologram insertLine(String line, int index) {
        this.lines.add(index, line);
        this.plugin.getHoloConfig().set("holo." + this.name + ".lines", this.lines);
        this.plugin.getHoloConfig().save();
        return this;
    }

    public Hologram update() {
        for (TextDisplay c : this.holos) {
            c.remove();
        }
        this.spawnHologram();
        return this;
    }

    public List<TextDisplay> spawnHologram() {
        List<TextDisplay> ret = new ArrayList<>();
        Location spawnLoc = this.location.clone();
        for (String line : this.lines) {
            TextDisplay c = this.spawnLine(spawnLoc, line);
            spawnLoc.add(0, this.lineSpacing, 0);
            this.holos.add(c);
            ret.add(c);
        }
        return ret;
    }

    public static void removeHologram(Hologram holo) {
        holo.plugin.getHoloConfig().saveSet("holo." + holo.getName(), null);
        holo.plugin.getHoloConfig().saveWithOutRegister();

        List<Hologram> tempHoloList = new ArrayList<>(Hologram.all);

        for (Hologram c : tempHoloList) {
            c.despawn();
            Hologram.all.remove(c);
        }

        holo.plugin.loadHologramsLater();
    }

    private TextDisplay spawnLine(Location lineLoc, String line) {
        TextDisplay text = (TextDisplay) lineLoc.getWorld().spawnEntity(lineLoc, EntityType.TEXT_DISPLAY);
        text.addScoreboardTag(this.name);
        text.addScoreboardTag("holographic_display");
        text.text(Component.text(line));
        text.setGravity(false);
        text.setBillboard(this.billboard);
        text.setDefaultBackground(this.background);
        text.setTextOpacity(this.opacity);
        text.setBackgroundColor(this.backgroundColor);
        text.setSeeThrough(this.seeThrough);
        text.setShadowed(this.shadow);
        text.setAlignment(this.textAlignment);
        return text;
    }

    public void despawn() {
        for (World w : Bukkit.getWorlds()) {
            for (Entity e : w.getEntities()) {
                if (e.getScoreboardTags().contains("holographic_display")) {
                    w.getEntities().remove(e);
                    e.remove();
                }
            }
        }

    }

    public void setBackground(boolean background, boolean config) {
        this.background = background;
        this.plugin.getHoloConfig().set("holo." + this.name + ".background", this.background);
        this.plugin.getHoloConfig().save();
    }

    public void setBackgroundColor(Color backgroundColor, boolean config) {
        this.backgroundColor = backgroundColor;
        this.plugin.getHoloConfig().set("holo." + this.name + ".backgroundColor", this.backgroundColor);
        this.plugin.getHoloConfig().save();
    }

    public void setBillboard(Display.Billboard billboard, boolean config) {
        this.billboard = billboard;
        this.plugin.getHoloConfig().set("holo." + this.name + ".billboard", this.billboard.name());
        this.plugin.getHoloConfig().save();
    }

    public void setLineSpacing(double lineSpacing, boolean config) {
        this.lineSpacing = lineSpacing;
        this.plugin.getHoloConfig().set("holo." + this.name + ".lineSpacing", this.lineSpacing);
        this.plugin.getHoloConfig().save();
    }

    public void setLine(String line, int index) {
        this.lines.set(index, line);
        this.plugin.getHoloConfig().setStringList("holo." + this.name + ".lines", this.lines.toArray(new String[lines.size()]));
        this.plugin.getHoloConfig().save();
    }

    public void setLocation(Location location, boolean config) {
        this.location = location;
        this.plugin.getHoloConfig().setLocation("holo." + this.name + ".location", this.location);
        this.plugin.getHoloConfig().save();
    }

    public void setOpacity(byte opacity, boolean config) {
        this.opacity = opacity;
        this.plugin.getHoloConfig().set("holo." + this.name + ".opacity", new Byte[]{this.opacity});
        this.plugin.getHoloConfig().save();
    }

    public void setSeeThrough(boolean seeThrough, boolean config) {
        this.seeThrough = seeThrough;
        this.plugin.getHoloConfig().set("holo." + this.name + ".seeThrough", this.seeThrough);
        this.plugin.getHoloConfig().save();
    }

    public void setShadow(boolean shadow, boolean config) {
        this.shadow = shadow;
        this.plugin.getHoloConfig().set("holo." + this.name + ".shadow", this.shadow);
        this.plugin.getHoloConfig().save();
    }

    public void setTextAlignment(TextDisplay.TextAlignment textAlignment, boolean config) {
        this.textAlignment = textAlignment;
        this.plugin.getHoloConfig().set("holo." + this.name + ".textAlignment", this.textAlignment.name());
        this.plugin.getHoloConfig().save();
    }

}



