package eu.filtastisch.holographicdisplaysreloaded.listener;

import de.thesourcecoders.capi.spigot.ItemBuilder;
import eu.filtastisch.holographicdisplaysreloaded.types.Hologram;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GuiListener implements Listener {

    @EventHandler
    public void onInvClick(InventoryClickEvent e){
        if (e.getView().title().equals(Component.text("§2Holograms"))){
            e.setCancelled(true);
            Player p = (Player) e.getWhoClicked();
            if (e.getCurrentItem() != null) {
                String name = e.getCurrentItem().getItemMeta().getDisplayName();
                Hologram holo = Hologram.getFromName(name.replaceAll("§a", ""));
                assert holo != null;
                this.openHoloConfigInv(p, holo);
            }
        }
    }

    public void openHoloConfigInv(Player p, Hologram holo){
        Inventory inv = Bukkit.createInventory(p, 9*6, Component.text("§2Edit §c§l" + holo.getName()));
        ItemStack filler = new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setName(" ").create();

        int rows = inv.getSize() / 9;

        int i;
        for(i = 0; i < 9; ++i) {
            inv.setItem(i, filler);
            inv.setItem(i + inv.getSize() - 9, filler);
        }

        for(i = 0; i < rows; ++i) {
            inv.setItem(9 * i, filler);
            inv.setItem(9 * i + 8, filler);
        }

        for (Hologram c : Hologram.all){
            ItemStack item = new ItemBuilder(Material.PAPER).setName("§a" + c.getName()).create();
            inv.addItem(item);
        }

        ItemStack backGroundColor = new ItemBuilder(Material.INK_SAC).setName("§3Select Background Color").create();
        ItemStack moveHereItem = new ItemBuilder(Material.ENDER_PEARL).setName("§dMove here").create();
        ItemStack shadowItem =
                holo.isShadow() ?
                        new ItemBuilder(Material.RED_DYE).setName("§aEnable Shadow").create() :
                        new ItemBuilder(Material.LIME_DYE).setName("§aDisable Shadow").create();

        ItemStack seeThroughItem =
                holo.isSeeThrough() ?
                        new ItemBuilder(Material.RED_DYE).setName("§aEnable See Through").create() :
                        new ItemBuilder(Material.LIME_DYE).setName("§aDisable See Through").create();

        inv.setItem(10, backGroundColor);
        inv.setItem(19, moveHereItem);
        inv.setItem(11, shadowItem);
        inv.setItem(12, seeThroughItem);
        p.openInventory(inv);
    }

}
