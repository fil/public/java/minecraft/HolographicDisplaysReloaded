package eu.filtastisch.holographicdisplaysreloaded.listener;

import eu.filtastisch.holographicdisplaysreloaded.types.Hologram;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.Objects;

public class ClickEventListener implements Listener {

    @EventHandler
    public void onCommandPre(PlayerCommandPreprocessEvent e) {
        String msg = e.getMessage();
        String[] args = msg.split(" ");
        Player p = e.getPlayer();
        if (msg.startsWith("/holographicdisplays_9DxkiUmZxH1kukVObceOhJGlSgm5Z5")) {
            e.setCancelled(true);
            p.teleport(Objects.requireNonNull(Hologram.getFromName(args[1])).getLocation());
        }
    }

}
