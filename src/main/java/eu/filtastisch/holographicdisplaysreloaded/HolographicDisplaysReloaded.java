package eu.filtastisch.holographicdisplaysreloaded;

import de.thesourcecoders.capi.spigot.GuiManager;
import de.thesourcecoders.capi.spigot.SpigotConfig;
import eu.filtastisch.holographicdisplaysreloaded.commands.HologramCommand;
import eu.filtastisch.holographicdisplaysreloaded.commands.HologramTabComplete;
import eu.filtastisch.holographicdisplaysreloaded.listener.ClickEventListener;
import eu.filtastisch.holographicdisplaysreloaded.listener.GuiListener;
import eu.filtastisch.holographicdisplaysreloaded.types.Hologram;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Display;
import org.bukkit.entity.TextDisplay;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class HolographicDisplaysReloaded extends JavaPlugin {

    @Getter
    private static HolographicDisplaysReloaded instance;

    @Getter
    private SpigotConfig defaultConfig, holoConfig;

    @Getter
    private final String prefix = "§bHolograficDisplays §7| ";

    @Getter
    private String textAlign, billboard;
    @Getter
    private boolean shadow, background, seeThrough;
    @Getter
    private double lineSpacing;
    @Getter
    private int backgroundColor;
    @Getter
    private List<Byte> opacity;
    @Getter
    private List<String> holoNameList;

    @Override
    public void onEnable() {
        instance = this;
        this.holoNameList = new ArrayList<>();
        GuiManager.registerGUIManager(this);
        this.registerConfigs();
        this.loadHologramsLater();
        Bukkit.getPluginManager().registerEvents(new GuiListener(), this);
        Bukkit.getPluginManager().registerEvents(new ClickEventListener(), this);
        Objects.requireNonNull(this.getCommand("holographicdisplay")).setExecutor(new HologramCommand());
        Objects.requireNonNull(this.getCommand("holographicdisplay")).setTabCompleter(new HologramTabComplete());
        this.loadValues();
    }

    @Override
    public void onDisable() {
        //this.saveHolograms();
        this.removeHolos();
    }

    public void registerConfigs(){
        this.defaultConfig = new SpigotConfig(this, "config.yml").register();
        this.holoConfig = new SpigotConfig(this, "data.yml").register();

        this.defaultConfig.addDefault("holodefault.opacity", new Byte[]{0});
        this.defaultConfig.addDefault("holodefault.shadow", false);
        this.defaultConfig.addDefault("holodefault.background", true);
        this.defaultConfig.addDefault("holodefault.seeThrough", true);
        this.defaultConfig.addDefault("holodefault.textAlignment", "CENTER");
        this.defaultConfig.addDefault("holodefault.backgroundColor", 16777215);
        this.defaultConfig.addDefault("holodefault.lineSpacing", -0.3);
        this.defaultConfig.addDefault("holodefault.billboard", "CENTER");
        this.defaultConfig.registerDefaults();
        this.holoConfig.registerDefaults();
    }

    public void loadValues() {
        this.opacity = this.defaultConfig.getByteList("holodefault.opacity");
        this.shadow = this.defaultConfig.getBoolean("holodefault.shadow");
        this.background = this.defaultConfig.getBoolean("holodefault.background");
        this.seeThrough = this.defaultConfig.getBoolean("holodefault.seeThrough");
        this.textAlign = this.defaultConfig.getString("holodefault.textAlignment");
        this.billboard = this.defaultConfig.getString("holodefault.billboard");
        this.backgroundColor = this.defaultConfig.getInt("holodefault.backgroundColor");
        this.lineSpacing = this.defaultConfig.getDouble("holodefault.lineSpacing");
    }

    public void loadHologramsLater() {
        new BukkitRunnable() {
            @Override
            public void run() {
                loadHolograms();
            }
        }.runTaskLater(this, 20 * 2);
    }

    public void loadHolograms() {
        if (this.holoConfig.getConfigurationSection("holo") == null)
            return;

        this.holoNameList.clear();

        for (String key : this.holoConfig.getConfigurationSection("holo").getKeys(false)) {
            Location loc = this.holoConfig.getLocation("holo." + key + ".location");
            List<String> lines = this.holoConfig.getStringList("holo." + key + ".lines");
            List<Byte> opacity = this.holoConfig.getByteList("holo." + key + ".opacity");
            boolean shadow = this.holoConfig.getBoolean("holo." + key + ".shadow");
            boolean back = this.holoConfig.getBoolean("holo." + key + ".background");
            boolean see = this.holoConfig.getBoolean("holo." + key + ".seeThrough");
            String alignment = this.holoConfig.getString("holo." + key + ".textAlignment");
            TextDisplay.TextAlignment textAlignment = this.getAlignment(alignment);
            Color color = Color.fromARGB(this.holoConfig.getInt("holo."+key+".backgroundColor"));
            double lineSpace = this.holoConfig.getDouble("holo." + key + ".lineSpacing");
            Display.Billboard billboard = Display.Billboard.valueOf(this.holoConfig.getString("holo."+key+".billboard"));

            Hologram holo = new Hologram(key, loc, lines, lineSpace, false);

            holo.setOpacity(opacity.get(0));
            holo.setShadow(shadow);
            holo.setBackground(back);
            holo.setSeeThrough(see);
            holo.setTextAlignment(textAlignment);
            holo.setBackgroundColor(color);
            holo.setBillboard(billboard);

            holo.spawnHologram();
        }
    }

    /*
        public void saveDisableHolograms(){
            for (Hologram holo : Hologram.all){
                String name = holo.getName();
                this.holoConfig.setLocation("holo." + name+".location", holo.getLocation());
                this.holoConfig.set("holo." + name+".lines", holo.getLines());
                this.holoConfig.set("holo." + name+".opacity", new Byte[] {holo.getOpacity()});
                this.holoConfig.set("holo." + name+".shadow", holo.isShadow());
                this.holoConfig.set("holo." + name+".background", holo.isBackground());
                this.holoConfig.set("holo." + name+".seeThrough", holo.isSeeThrough());
                this.holoConfig.set("holo." + name+".textAlignment", holo.getTextAlignment().name());
                this.holoConfig.set("holo." + name+".backgroundColor", holo.getBackgroundColor().asARGB());
                this.holoConfig.set("holo." + name+".lineSpacing", holo.getLineSpacing());
                this.holoConfig.set("holo." + name+".billboard", holo.getBillboard().name());
                holo.despawn();
            }
            this.holoConfig.save();
        }

        public void saveHolograms(){
            for (Hologram holo : Hologram.all){
                String name = holo.getName();
                this.holoConfig.setLocation("holo." + name+".location", holo.getLocation());
                this.holoConfig.set("holo." + name+".lines", holo.getLines());
                this.holoConfig.set("holo." + name+".opacity", new Byte[] {holo.getOpacity()});
                this.holoConfig.set("holo." + name+".shadow", holo.isShadow());
                this.holoConfig.set("holo." + name+".background", holo.isBackground());
                this.holoConfig.set("holo." + name+".seeThrough", holo.isSeeThrough());
                this.holoConfig.set("holo." + name+".textAlignment", holo.getTextAlignment().name());
                this.holoConfig.set("holo." + name+".backgroundColor", holo.getBackgroundColor().asARGB());
                this.holoConfig.set("holo." + name+".lineSpacing", holo.getLineSpacing());
                this.holoConfig.set("holo." + name+".billboard", holo.getBillboard().name());
            }
            this.holoConfig.save();
        }
     */
    public void removeHolos(){
        for (Hologram holo : Hologram.all)
            holo.despawn();
    }

    public TextDisplay.TextAlignment getAlignment(String name){
        if (name.equals("CENTER"))
            return TextDisplay.TextAlignment.CENTER;
        if (name.equals("LEFT"))
            return TextDisplay.TextAlignment.LEFT;
        if (name.equals("RIGHT"))
            return TextDisplay.TextAlignment.RIGHT;

        return TextDisplay.TextAlignment.CENTER;
    }

}
